#!/usr/bin/env pythoh3

'''
Maneja un diccionario con los artículos de una tienda y
sus precios, y permite luego comprar alguno de sus elementos,
y ver el precio total de la compra.
'''
import sys

articulos = {"leche":1,"pan":0.75, "patatas":2.20}

def anadir(articulo, precio):
    """Añade un artículo y un precio al diccionario artículos"""
    articulos[articulo]=precio

def mostrar():
    """Muestra en pantalla todos los artículos y sus precios"""
    print("Lista de artículos en la tienda: ")
    for articulo, precio in articulos.items():
        print(f"{articulo}: {precio} euros")

def pedir_articulo() -> str:
    """Pide al usuario el artículo a comprar.

    Muestra un mensaje pidiendo el artículo a comprar.
    Cuando el usuario lo escribe, comprueba que es un artículo qie
    está en el diccionario artículos, y termina devolviendo ese
    artículo como string. Si el artículo escrito por el usuario no
    está en artículos, pide otro hasta que escriba uno que sí está."""
    while True:
        articulo= input("Artículo: ")
        if articulo in articulos:
            return articulo

def pedir_cantidad() -> float:
    """Pide al usuario la cantidad a comprar.

    Muestra un mensaje pidiendo la cantidad a comprar.
    Cuando el usuario lo escribe, comprueba que es un número real
    (float), y termina devolviendo esa cantidad.. Si la cantidad
    escrita no es un float, pide otra hasta que escriba una correcta.
    """
    while True:
        x =input("Cantidad: ")
        try:
            cantidad=float(x)
            return cantidad
        except ValueError:
            print("Cantidad no válida.")
def main():
    """Toma los artículos declarados como argumentos en la línea de comando,
    junto con sus precios, almacénalos en el diccionario artículos mediante
    llamadas a la función añadir. Luego muestra el listado de artículos
    y precios para que el usuario pueda elegir. Termina llamando a las
    funciones pedir_articulo y pedir cantidad, y mostrando en pantalla la
    cantidad comprada, de qué artículo, y cuánto es su precio.
    """
    for i in range(1, len(sys.argv),2):
        articulo= sys.orig_argv[i]
        precio=float(sys.argv[i+1])
        agrega(articulo,precio)
    mostrar()

    comprado= pedir_articulo()
    cantidad_de_cosas= pedir_cantidad()
    p = cantidad_de_cosas*articulos[comprado]
    print(f"\nCompra total: {cantidad_de_cosas} de {comprado}, a pagar {p}")
if __name__ == '__main__':
    main()
